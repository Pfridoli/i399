import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {NewComponent} from "./new/new.component";
import {SearchComponent} from "./search/search.component";
import {ContactService} from "./contact.service";
import {SearchPipe} from "./filter.pipe";


@NgModule({
    declarations: [
        AppComponent,
        SearchComponent,
        NewComponent,
        SearchPipe
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule,
    ],
    providers: [ContactService],
    bootstrap: [AppComponent]
})
export class AppModule {
}

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ContactService} from "../contact.service";
import {Contact} from "../contact";

@Component({
    selector: 'app-new',
    templateUrl: 'new.html',
    styles: []
})

export class NewComponent implements OnInit {

    constructor(private contactService: ContactService, private router: Router, private route: ActivatedRoute) {
    }

    public person;

    addNewContact() {
        if (this.person._id) {
            this.contactService.putContact(this.person)
                .then(() => {
                    this.router.navigateByUrl('/search');
                });
        } else {
            this.contactService.postContact(new Contact(this.person.name, this.person.phone))
                .then(() => {
                    this.router.navigateByUrl('/search');
                });
        }
    }

    idGetPerson() {
        let personID = this.route.snapshot.params.id;

        if (!personID) {
            this.person = {};
            return;
        }

        this.contactService.getContact(personID)
            .then((test) => {
                this.person = test;
            })
    }

    ngOnInit() {
        this.idGetPerson()
    }

}


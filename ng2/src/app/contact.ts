export class Contact {
    public _id: number;
    public name: string;
    public phone: string;
    public selected: boolean = false;


    constructor(name: string, phone: string) {
        this.name = name;
        this.phone = phone;
    }
}

import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Contact} from "./contact";
import {errorHandler} from "@angular/platform-browser/src/browser";

@Injectable()
export class ContactService {

    constructor(private http: HttpClient) {
    }

    public getContacts(): Promise<Contact[]> {
        return this.http.get('/contacts')
            .toPromise()
            .then(result => <Contact[]> result);
    }

    public getContact(id): Promise<Contact[]> {
        return this.http.get('/contacts/' + id)
            .toPromise()
            .then(result => <Contact[]> result);
    }

    public putContact(contact): Promise<void> {
        return this.http.put('/contacts/' + contact._id, contact)
            .toPromise()
            .then(() => <void> null);
    }

    public postContact(contact: Contact): Promise<void> {
        return this.http.post('/contacts', contact)
            .toPromise()
            .then(() => <void> null);
    }

    public deleteContact(id): Promise<void> {
        return this.http.delete('/contacts/' + id)
            .toPromise()
            .then(() => <void> null);
    }

}

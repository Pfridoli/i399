import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SearchComponent} from "./search/search.component";
import {NewComponent} from "./new/new.component";

const routes: Routes = [
    {path: 'search', component: SearchComponent},
    {path: 'new', component: NewComponent},
    {path: 'new/:id', component: NewComponent},
    {path: '', redirectTo: 'search', pathMatch: 'full'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

import {Component, OnInit} from '@angular/core';
import {Contact} from "../contact";
import {ContactService} from "../contact.service";

@Component({
    selector: 'app-search',
    templateUrl: 'search.html',
    styles: []
})
export class SearchComponent implements OnInit {

    constructor(private contactService: ContactService) {
        this.getData();
    }

    contacts: Contact[];
    query: any;

    getData() {
        this.contactService.getContacts()
            .then(contacts => this.contacts = contacts);
    }

    deleteContact(id) {
        this.contactService.deleteContact(id)
            .then(() => {
                this.getData()
            })
    }

    deleteContactSelected() {
        this.contacts = this.contacts.filter(_ => _.selected);

        if (this.contacts.length == 0 || this.contacts === undefined) {
            this.getData();
            return;
        }

        for (let contact in this.contacts) {
            this.contactService.deleteContact(this.contacts[contact]._id)
        }
        this.getData();
    }

    ngOnInit() {
        this.getData();
    }

}
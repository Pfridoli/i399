'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const mongoDB = require('mongodb');

const Dao = require('./dao.js');

const dao = new Dao();
const app = express();

let url = 'mongodb://user:12345@ds249718.mlab.com:49718/i399';

app.use(bodyParser.json());
app.use(express.static('./'));
app.use(errorHandler);

app.get('/api/contacts', getContacts);
app.post('/api/contacts', addContact);
app.get('/api/contacts/:id', getContact);
app.delete('/api/contacts/:id', deleteContact);
app.post('/api/contacts/delete', deleteMultipleContacts);
app.put('/api/contacts/:id', changeContact);


dao.connect(url).then(() => {
    app.listen(3000, () => console.log('Server is running on port 3000'))
}).catch(err => {
    console.log("Connection failed: ");
    console.log(err);
    process.exit(1);
});


function getContacts(request, response, next) {
    dao.findAll()
        .then(contacts => response.json(contacts))
        .catch(next);
}

function getContact(request, response, next) {
    let id = request.params.id;

    dao.findById(id)
        .then(contacts => response.json(contacts))
        .catch(next);
}

function addContact(request, response, next) {
    let contact = request.body;

    let contactData = {
        "name": contact.name,
        "phone": contact.phone
    };

    dao.insert(contactData)
        .then(contacts => response.json(contacts))
        .catch(next);

}

function deleteContact(request, response, next) {
    let id = request.params.id;

    dao.remove(id)
        .then(() => response.end())
        .catch(next);
}

// https://stackoverflow.com/questions/2421595/restful-way-for-deleting-a-bunch-of-items/2421643#2421643
function deleteMultipleContacts(request, response, next) {
    let ids = request.body;

    dao.removeMultiple(ids)
        .then(contacts => response.json(contacts))
        .catch(next);
}

function changeContact(request, response, next) {
    let id = request.params.id;
    let contact = request.body;

    let contactData = {
        "name": contact.name,
        "phone": contact.phone
    };

    dao.update(id, contactData)
        .then(contacts => response.json(contacts))
        .catch(next)

}

function errorHandler(error, request, response, next) {
    console.log(error);
    response.status(500).json({error2: error.toString()});
}
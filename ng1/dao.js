'use strict';

const mongodb = require('mongodb');
const ObjectID = require('mongodb').ObjectID;
const COLLECTION = 'test-collection';

class Dao {

    connect(url) {
        return mongodb.MongoClient.connect(url)
            .then(db => this.db = db);
    }

    findAll() {
        return this.db.collection(COLLECTION).find().toArray();
    }

    findById(id) {
        id = new ObjectID(id);
        return this.db.collection(COLLECTION).findOne({_id:id});
    }

    insert(data) {
        return this.db.collection(COLLECTION).insertOne(data);
    }

    remove(id) {
        id = new ObjectID(id);
        return this.db.collection(COLLECTION).remove({_id:id});
    }

    removeMultiple(idList) {
        var ids = [];
        for (let id of idList) {
            ids.push(new ObjectID(id))
        }

        return this.db.collection(COLLECTION).remove({_id: {$in: ids}});
    }


    update(id, data) {
        id = new ObjectID(id);
        data._id = id
        return this.db.collection(COLLECTION)
            .updateOne({_id:id}, data);
    }

    close() {
        if (this.db) {
            this.db.close();
        }
    }
}

module.exports = Dao;
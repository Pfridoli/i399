(function () {
    'use strict';

    angular.module("app").controller("NewCtrl", Controller);

    function Controller($http, $routeParams, $location) {
        let vm = this;
        vm.person = {};

        getPerson($routeParams.id);

        function getPerson(id) {
            if (!id) {
                return;
            }
            console.log(id);
            $http.get('api/contacts/' + id).then(rece =>
                vm.person = rece.data
            );
        }

        this.addItem = () => {
            console.log(vm.person);
            if (vm.person._id) {
                $http.put('api/contacts/' + vm.person._id, vm.person).then(() =>
                    $location.path("/search")
                );
            } else {
                $http.post('api/contacts', vm.person).then(() =>
                    $location.path("/search")
                );
            }
        };
    }
})();
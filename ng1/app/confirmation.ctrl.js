(function () {
    'use strict';

    angular.module("app").service("modalService", Controller);

    function Controller($q, $document, $http, $compile, $rootScope) {

        let modal;
        let scope;

        this.confirm = function () {
            return $http.get('app/confirmation.html').then(result => {
                createModal(result.data);
                return showModel();
            });

            function showModel() {
                let deferred = $q.defer();

                scope.ok = () => {
                    modal.addClass('hide');
                    deferred.resolve();
                };

                scope.cancel = () => {
                    modal.addClass('hide');
                    deferred.reject();
                };

                modal.removeClass('hide');

                return deferred.promise;
            }
        };

        function createModal(html) {
            let body = angular.element($document[0].body);
            let containerElement = angular.element(html);
            scope = $rootScope.$new();
            modal = $compile(containerElement[0])(scope);
            body.append(modal);
        }
    }
})();
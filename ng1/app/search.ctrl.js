(function () {
    'use strict';

    angular.module("app").controller("SearchCtrl", Controller);

    function Controller($http, modalService) {
        let vm = this;
        this.contacts = [];

        getContacts();

        function getContacts() {
            $http.get("/api/contacts").then(result =>
                vm.contacts = result.data
            );
        }

        this.deleteContact = id => {
            modalService.confirm().then(() =>
                $http.delete("api/contacts/" + id).then(() =>
                    getContacts()
                )
            );
        };
    }
})();